INCLUDE_DIRECTORIES("${PROJECT_INCLUDE_DIR}")

SET(CLIENTE_SRCS 
	MundoCliente.cpp 
	Esfera.cpp
	Plano.cpp
	Raqueta.cpp
	Vector2D.cpp)
		
SET(SERVER_SRCS
	MundoServer.cpp
	Esfera.cpp
	Plano.cpp
	Raqueta.cpp
	Vector2D.cpp)
ADD_EXECUTABLE(cliente cliente.cpp ${CLIENTE_SRCS})
ADD_EXECUTABLE(server server.cpp ${SERVER_SRCS})		
ADD_EXECUTABLE(logger logger.cpp)
ADD_EXECUTABLE(bot bot.cpp)

TARGET_LINK_LIBRARIES(server glut GL GLU)
TARGET_LINK_LIBRARIES(cliente glut GL GLU)
